﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DateTime thisday = DateTime.Today;
            Console.WriteLine("Today is " + (thisday.ToString("D")));
        }
    }
}
